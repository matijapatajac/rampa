var express = module.require('express');
var app = express();
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var mysql = require('mysql');

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname + '/static'));

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root', 
  password: '',
  database: 'rampa'
});

var secret = "tajna";
var users = [
  {email: 'a', pass: 'a'},
  {email: 'portir', pass: 'rampa'}
];

app.listen(2607, function(){
  console.log("Listening on 2607");
});

connection.connect(function(err){
  if (err){
    console.log("Greška u spajanju s bazom podataka: ", err);
  } else {
    console.log("Uspješno spajanje s bazom podataka!");
  }
});

var apiRoute = express.Router();

connection.query('SELECT * FROM termini',function(err,rows){
  if(err) console.log("Greška: ", err);

  console.log('Podaci iz baze: \n');
  console.log(rows);
});

//--------------------------------------------------------------------------------


var termini = [];

app.post("/termin", function(req,res){
  console.log(req.body.korisnik)
  var ucitaj = "SELECT * FROM termini;";
  console.log(ucitaj);
  connection.query(ucitaj,function(err,rows){
  if(err) console.log(err);
  console.log('Data received from Db: \n');
  console.log(rows); 
  termini = rows;
    });
    res.status(200).json(termini);
});

  app.post("/brisanje", function(req,res){
  var brisanje = "DELETE FROM termini;" ;
  console.log(brisanje);
  connection.query(brisanje,function(err,rows){
  if(err) console.log(err);
  console.log('Data received from Db: \n');
  console.log(rows); 
    });
});



  app.post("/posalji", function(req,res){
  var slanje = "INSERT INTO termini VALUES (null,'" + req.body.gost + "','"+req.body.vrijeme + "','" + req.body.datum + "','" + req.body.napomena + "');" ;
  console.log(slanje);
  connection.query(slanje,function(err,rows){
  if(err) console.log(err);
  console.log('Data received from Db: \n');
  console.log(rows); 
    });
});
/*
app.post("/rasporedupdate", function(req,res){
var quer4 = "UPDATE rokovnik SET Korisnik='" + req.body.korisnik2 + "',Dan='"+req.body.dan1 + "',Biljeska='" + req.body.biljeska1 + "' WHERE Id='"+req.body.id1 + "';" ;
console.log(quer4);
connection.query(quer4,function(err,rows){
if(err) throw err;
console.log('Data received from Db: \n');
console.log(rows); 
  });
});

*/
apiRoute.post('/procitaj', function(req, res) { 
var rd = "SELECT * FROM termini;" ;
connection.query(rd,function(err,result, fields){
  if(err != null){
    console.log("Greška: ", err);
  } else {
    console.log("Korisnici: ", result);
    resultPrimary = result;
     res.status(200).json(result);
  };

});

 
});

//-----------------------------------------------------------------------

app.post('/auth', function(req,res){
  var status = 401;
  var response = {"success": false};

  var user = getUser(req.body.email, req.body.pass);
  if(user != null){
    var token = jwt.sign(user,secret);
    response.success = true;
    response.token = token;
    status =200;
  }
  res.status(status).json(response);

});



apiRoute.use(function (req,res,next) {
  var token = req.headers['x-auth-token'];
  if(token){
    jwt.verify(token, secret, function(err,payLoad){
      if(err){
        return res.status(400).json({success:false, message:"Krivi token."});    
      }else{
        next();
      }
    });
  }else{
    return res.status(400).json({success:false, message:"Nedostaje token."});
  }
});
apiRoute.get('/users', function(req,res){
  res.status(200).json(users);
});
app.use('/api',apiRoute);


app.post('/register', function(req, res){
  var email = req.body.email;
  var pass = req.body.pass;
  var success = false;
    if (!userExists(email)) {
      var user = {'email': email, 'pass': pass};
      users.push(user);
      success = true;
    }
    var status = success? 200: 400;
    res.status(status).json({"success":success});
});

//--------------------------------------------------------------------

function getUser(email,pass){
  for(var i=0; i<users.length; i++){
    if(users[i].email == email && users[i].pass == pass){
      return users[i];
    }
  }
  return null;
}
function verifyLogin(email, pass){
  for(var i=0; i<users.length; i++){
    if(users[i].email == email && users[i].pass == pass){
      return true;
    }
  }
  return false;
}

function userExists(email) {
  for(var i=0; i<users.length; i++){
    if(users[i].email==email){
      return true;
    }
  }
  return false;
}