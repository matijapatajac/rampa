var app = angular.module('projekt');

app.controller('LoginController'
  , function($rootScope, $scope, $http){
  
    var self = this;
  this.email = "prazno";
  this.pass = "prazno";
  $rootScope.portirLoginSuccess = false;
  $rootScope.profLoginSuccess = false;

  this.send = function(email, pass){
    self.email = email;
    self.pass = pass;

    var data = {email:email, pass:pass};
    $http({
      data:data,
      method:'POST',
      url:'/auth'
    }).then(function successCallback(response){
      console.log("response", response);
      $rootScope.portirLoginSuccess = true;
      $rootScope.profLoginSuccess = true;
      $rootScope.token = response.data.token;
    }, function errorCallback(response){
      console.log("greska");
      $rootScope.portirLoginSuccess = false;
      $rootScope.profLoginSuccess = false;
      alert("Krivo ime i/ili lozinka!");
    });

  };
  this.logOut = function logOut(){
    console.log("odjava");
    $rootScope.portirLoginSuccess = false;
    $rootScope.profLoginSuccess = false;
  };
});