var app = angular.module('projekt');

app.controller('TerminController'
  , function($rootScope, $scope, $http){
  
    var self = this;
  this.gost = "prazno";
  this.vrijeme = "prazno";
  this.datum = "prazno";
  this.napomena = "prazno";
  this.korisnik = "portir";
  $rootScope.terminSuccess = false;

  this.send = function(gost, vrijeme, datum, napomena){
    self.gost = gost;
    self.vrijeme = vrijeme;
    self.datum = datum;
    self.napomena = napomena;

    $rootScope.gost = gost;
    $rootScope.vrijeme = vrijeme;
    $rootScope.datum = datum;
    $rootScope.napomena = napomena;

    var data = {'gost':gost, 'vrijeme':vrijeme, 'datum':datum, 'napomena':napomena};
    $http({
      data:data,
      method:'POST',
      url:'/posalji'
    })
    .then(function successCallback(response){
      console.log("response", response);
      //alert("Termin uspješno poslan!");
      $rootScope.terminSlanje = response;
      $rootScope.terminSuccess = true;
    }, function errorCallback(response){
      console.log("greska", response);
      $rootScope.terminSuccess = false;
    });

  };

  this.dodaj = function dodaj(){
    console.log("novi termin");
    $rootScope.terminSuccess = false;
  };

  $scope.read = [];
  this.dohvati = function(korisnik){
     var data = {'korisnik': korisnik};
    $http({
      method: 'POST',
        url: '/api/procitaj',
        data: data
      }).then(function successCallback(response){
        $scope.read = response;
        console.log($scope.read);
        })
      }, function errorCallback(response) {
        console.log("GRESKA:", error);
    }

    this.brisanje = function(id){
    $http({
      //data: data,
      method: 'POST',
      url: '/brisanje'
    }).then(function successCallback(response){
      console.log("response", response);
      $rootScope.brisanje = response;
      console.log(response);
    }, function errorCallback(response) {
      console.log("GRESKA", response);     
      
    });
  }
});