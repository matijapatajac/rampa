var app = angular.module('projekt');

app.controller('RegisterController'
  , function($rootScope, $scope, $http){
  
    var self = this;
  this.email = "prazno";
  this.pass = "prazno";
  this.key = "prazno";
  $rootScope.registerSuccess = false;

  this.send = function(email, pass, key){
    self.email = email;
    self.pass = pass;
    self.key = key;

    var data = {email:email, pass:pass, key:key};
    $http({
      data:data,
      method:'POST',
      url:'/register'
    }).then(function successCallback(response){
      console.log("response", response);
      if (self.key == "pitagora"){
      $rootScope.registerSuccess = true;
    } else {
      $rootScope.registerSuccess = false;
      alert("Kriva ključna riječ!");
    }
    }, function errorCallback(response){
      console.log("GRESKA");
      $rootScope.registerSuccess = false;
    });

  };

});