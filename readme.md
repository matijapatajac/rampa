#**Rampa**

##Kratki opis
Web aplikacija kojom će zaposlenici fakulteta moći obavijestiti portira o tome kada će biti potrebno podići rampu.

##Portir
###Prijava 
Prijava portira u sustav.

*Ruta*
```
POST /pocetna/portir/
```
*Zahtjev*
```
{
	"ime": portirIme, 
	"lozinka": portirLozinka
}
```
*Odgovor*
```
{
	"token": "lj02livo4bua9eln4uvom3hv20su5bi7"
}
```
---
###Prikupljanje upisanih termina
Portir dobiva popis termina kada može očekivati goste.

*Ruta*
```
GET /pocetna/portir/termini/
```
*Zahtjev*
```
{
	"termini": []
}
```
*Odgovor*
```
{
	"termini": [
		{ "termin": [...]},
		{ "termin": [...]},
		{ "termin": [...]},
	]
}
```
---
##Zaposlenici
###Registracija 
 
Registriranje zaposlenika u sustav.

*Ruta*
```
POST /pocetna/prof/reg/
```
*Zahtjev*
```
{
	"ime": profIme,
	"lozinka": profLozinka,
	"krijec": "pitagora"
}
```
*Odgovor*
```
{
	"odgovor": odgovor
}
```
---
###Prijava 
Prijava zaposlenika u sustav.

*Ruta*
```
POST /pocetna/prof/prijava/
```
*Zahtjev*
```
{
	"ime": profIme,
	"lozinka": profLozinka
}
```
*Odgovor*
```
{
	"token": "lj02livo4bua9eln4uvom3hv3n92vl3g"
}
``` 
---

###Slanje novog termina
Zaposlenik portiru šalje novi termin kada može očekivati gosta.

*Ruta*
```
POST /pocetna/prof/unos/
```
*Zahtjev*
```
{
	"ime": ime,  
	"vrijeme": vrijeme, 
	"napomena": napomena
}
```
*Odgovor*
```
{
	"odgovor": odgovor	
}
```
---
*©  Matija Patajac, 2016.*